#-SolutionDir $(SolutionDir) -ProjectDir $(ProjectDir) -ProjectPath $(ProjectPath) -ConfigurationName $(ConfigurationName) -TargetName $(TargetName) -TargetDir $(TargetDir) -ProjectName $(ProjectName) -PlatformName $(PlatformName) -TargetPath $(TargetPath)

param([string]$SolutionDir,
     [string]$ProjectDir,
     [string]$ProjectPath,
     [string]$ConfigurationName,
     [string]$TargetName,
     [string]$TargetDir,
     [string]$ProjectName,
     [string]$PlatformName,
     [string]$TargetPath);

# imports
. "${SolutionDir}scripts\shared.ps1"

<#
Package-NuSpec($Version) {
    $vChanges = Get-Content -path "${SolutionDir}changelog\vNext.md" -Raw
    $nuspecContents = Get-Content -path "${SolutionDir}${TargetName}.nuspec" -Raw

    $nuspecContents = $nuspecContents -replace '\$VERSION\$',"${$Version.assemblyVersion}"
    $nuspecContents = $nuspecContents -replace '\$RELEASENOTES\$',"$vChanges"
    $nuspecContents = $nuspecContents -replace 'bin\\x86\\Release\\','bin\x86\Debug\'

    Set-Content -Path "${SolutionDir}${TargetName}-TMP.nuspec" -Value $nuspecContents

    nuget.exe pack "${SolutionDir}${TargetName}-TMP.nuspec" -NonInteractive -OutputDirectory "${SolutionDir}bin\packages\"
}

# actual pre-build stuff
$version = Get-ProjectSemVer

if ($ConfigurationName -eq "Debug") {
    Package-NuSpec -Version $version
}
#>

Show-LocalDependencyIssues
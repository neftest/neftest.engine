<#
    Uses GitVersion to get the current semver for the project.

    If gitversion.exe is not found in the path, will default to 1.0.0-local-no-gitversion.1
#>
function Get-ProjectSemVer() {
    $version = @{
        assemblyVersion = "1.0.0"
        assemblyFileVersion = "1.0.0"
        informationalVersion = "1.0.0-local-no-gitversion.1"
        semVer = "1.0.0-local-no-gitversion.1"
    }

    if (Get-Command "gitversion.exe" -ErrorAction SilentlyContinue)  {
        $gitversion = (gitversion.exe | ConvertFrom-Json)
        $version.assemblyVersion = $gitversion.MajorMinorPatch
        $version.assemblyFileVersion = $gitversion.MajorMinorPatch
        $version.informationalVersion = $gitversion.InformationalVersion
        $version.semVer = $gitversion.SemVer
    }

    return $version
}

function Show-LocalDependencyIssues() {
    if (!(Get-Command "gitversion.exe" -ErrorAction SilentlyContinue))  {
        Write-Host "!!"
        Write-Host "!! gitversion.exe was not found in the path, will use default versioning!"
        Write-Host "!!"
    }
}

﻿using NefTest.Common.Enums;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Allows this item to be filtered
    /// </summary>
    public interface IFilterable {
        /// <summary>
        /// The Environment this belongs to
        /// </summary>
        EnvironmentType Environment { get; }
    }
}

﻿using NefTest.Engine.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Provides an interface for logging messages
    /// </summary>
    public interface ILogger {
        /// <summary>
        /// Log a message at the specified log level
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="logLevel">loglevel</param>
        void Log(string message, LogLevel logLevel = LogLevel.Info);

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="ex">The exception to log.</param>
        void Log(Exception ex);
    }
}

﻿using NefTest.Engine.Enums;
using System;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// A test item / container that can be reported on
    /// </summary>
    public interface IReportable {
        /// <summary>
        /// The duration this took (may be inferred from children)
        /// </summary>
        TimeSpan Duration { get; }

        /// <summary>
        /// The status of the test (may be inferred from children)
        /// </summary>
        TestStatus Status { get; }

        /// <summary>
        /// Calculates the total number of test cases contained within this item that match the specified status
        /// </summary>
        /// <param name="status">Status to filter test cases by</param>
        /// <returns>Total number of matching test cases</returns>
        int TotalCasesByStatus(TestStatus status);

        /// <summary>
        /// Calculates the total number of test cases contained within this item
        /// </summary>
        /// <returns>Total number of test cases</returns>
        int TotalCases();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface ITestReporter {
        void Write(ITestResult testResult);
    }
}

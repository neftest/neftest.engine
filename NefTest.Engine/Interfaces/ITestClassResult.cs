﻿using NefTest.Engine.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Represents the result of a test class
    /// </summary>
    public interface ITestClassResult : IReportable {
        /// <summary>
        /// Fully qualified name of the test class
        /// </summary>
        string FullyQualifiedName { get; }

        /// <summary>
        /// Short name of the test class
        /// </summary>
        string ShortName { get; }

        /// <summary>
        /// Reference to the ITestClass implementation that represents this test class
        /// </summary>
        ITestClass TestClass { get; }

        /// <summary>
        /// Gets all contained case results
        /// </summary>
        /// <returns></returns>
        IEnumerable<ITestCaseResult> GetCaseResults();

        /// <summary>
        /// Adds a test case result to this class result
        /// </summary>
        /// <param name="result"></param>
        void AddCaseResult(ITestCaseResult result);

        /// <summary>
        /// Checks if all contained test cases are passing
        /// </summary>
        /// <returns></returns>
        bool IsPassing();
    }
}

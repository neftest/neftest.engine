﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Used for exporting test results to a file
    /// </summary>
    public interface ITestExporter {
        /// <summary>
        /// Write the test results to the specified file
        /// </summary>
        /// <param name="testResult"></param>
        /// <param name="outFile"></param>
        void Write(ITestResult testResult, string outFile);
    }
}

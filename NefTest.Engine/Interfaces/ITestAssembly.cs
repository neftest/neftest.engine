﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Allows access to an assembly that contains test cases
    /// </summary>
    public interface ITestAssembly : IFilterable {
        /// <summary>
        /// The absolute file path of this assembly
        /// </summary>
        string AssemblyPath { get; set; }

        /// <summary>
        /// A reference to this assembly. Will be `null` if IsLoaded is false
        /// </summary>
        Assembly Assembly { get; set; }

        /// <summary>
        /// True is this assembly has been loaded
        /// </summary>
        bool IsLoaded { get; }

        /// <summary>
        /// Load the assembly
        /// </summary>
        /// <returns>bool status of loading the assembly</returns>
        bool Load();

        /// <summary>
        /// Get all testable classes contained within this assembly
        /// </summary>
        /// <returns></returns>
        IEnumerable<ITestClass> GetTestableClasses();

        /// <summary>
        /// Handles resolving any assemblies required by this assembly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns>The resolved assembly</returns>
        Assembly LoadAssemblyBytesFromTestDirectory(object sender, ResolveEventArgs args);
    }
}

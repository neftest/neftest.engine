﻿using System;
using System.Reflection;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Represents a specific test case. That is a single method with the 
    /// [[TestCase](xref:NefTest.Common.Attributes.TestCaseAttribute)] attribute contained
    /// inside of a valid [ITestClass](xref:NefTest.Engine.Interfaces.ITestClass).
    /// </summary>
    public interface ITestCase : IFilterable {
        /// <summary>
        /// Fully qualified name of this test case
        /// </summary>
        string FullyQualifiedName { get; }

        /// <summary>
        /// The MethodInfo of the test method
        /// </summary>
        MethodInfo MethodInfo { get; }

        /// <summary>
        /// The class this test case belongs to
        /// </summary>
        ITestClass TestClass { get; }

        /// <summary>
        /// Run the test case.
        /// </summary>
        /// <param name="testClassInstance">An instance of the test class containing this ITestCase.</param>
        /// <returns>ITestResult containing test case results.</returns>
        ITestCaseResult Run(object testClassInstance);
    }
}
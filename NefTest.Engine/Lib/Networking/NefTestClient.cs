﻿using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;

namespace NefTest.Engine.Lib.Networking {
    public class NefTestClient : ClientBase {
        public event EventHandler<RemoteClientConnectionEventArgs> OnRemoteClientConnected;
        public event EventHandler<RemoteClientConnectionEventArgs> OnRemoteClientDisconnected;

        public string Host { get; }
        public int Port { get; }
        public DateTime WorkerStartedAt { get; private set; }

        private BackgroundWorker worker;

        private double retrySeconds = 1;
        private DateTime lastRetry = DateTime.MinValue;

        public NefTestClient(string host, int port, ILogger log, Action<Action> runOnMainThread, SerializationBinder binder)
            : base(0, "local", log, runOnMainThread, binder) {
            Host = host;
            Port = port;

            OnMessageReceived += BaseClient_OnMessageReceived;
            StartBackgroundWorker();
        }

        private void BaseClient_OnMessageReceived(object sender, OnMessageEventArgs e) {
            switch (e.Header.Type) {
                case MessageHeaderType.RemoteClientConnected:
                    RunOnMainThread(() => {
                        OnRemoteClientConnected?.Invoke(this, new RemoteClientConnectionEventArgs(e.Header.SendingClientId, RemoteClientConnectionEventArgs.ConnectionType.Connected));
                    });
                    break;
                case MessageHeaderType.RemoteClientDisconnected:
                    RunOnMainThread(() => {
                        OnRemoteClientDisconnected?.Invoke(this, new RemoteClientConnectionEventArgs(e.Header.SendingClientId, RemoteClientConnectionEventArgs.ConnectionType.Disconnected));
                    });
                    break;
                case MessageHeaderType.ClientInit:
                    ClientId = e.Header.SendingClientId;
                    break;
                case MessageHeaderType.Serialized:
                    DeserializeMessage(e.Header, e.Body);
                    break;
            }
        }

        #region BackgroundWorker
        private void StartBackgroundWorker() {
            worker = new BackgroundWorker {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = false
            };
            worker.DoWork += BackgroundWorker_DoWork;
            worker.RunWorkerAsync();
            WorkerStartedAt = DateTime.UtcNow;
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = sender as BackgroundWorker;
            while (worker.CancellationPending != true) {
                try {
                    if (TcpClient == null && DateTime.UtcNow - lastRetry > TimeSpan.FromSeconds(retrySeconds)) {
                        retrySeconds *= 2;
                        //LogAction?.Invoke($"Attempting to connect to {Host}:{Port}");
                        lastKeepAliveRecv = DateTime.UtcNow;
                        lastKeepAliveSent = DateTime.UtcNow;
                        var client = new TcpClient(Host, Port);
                        ConnectionId = client.Client.LocalEndPoint.ToString();
                        SetClient(client);
                        retrySeconds = 1; // reset reconnection delay
                        lastRetry = DateTime.UtcNow;
                    }
                    if (TcpClient != null && TcpClient.Connected && DateTime.UtcNow - WorkerStartedAt > TimeSpan.FromSeconds(1)) {
                        if (DateTime.UtcNow - lastKeepAliveSent > TimeSpan.FromMilliseconds(3000)) {
                            //LogAction?.Invoke($"Sending keepalive from {Id}");
                            SendMessageBytes(new MessageHeader() {
                                SendingClientId = ClientId,
                                Type = MessageHeaderType.KeepAlive
                            }, new byte[] { });
                            lastKeepAliveSent = DateTime.UtcNow;
                        }
                        ReadIncoming();
                        WriteOutgoing();
                    }
                }
                catch (SocketException) {
                    //LogAction?.Invoke(ex.ToString());
                    TryLaunchServer();
                }
                catch (Exception ex) { LogAction?.Invoke(ex.ToString()); }
                Thread.Sleep(15);
            }
        }
        #endregion BackgroundWorker

        private void TryLaunchServer() {
            LogAction?.Invoke($"Trying to launch server...");
        }

        protected override void Dispose(bool isDisposing) {
            base.Dispose(isDisposing);
            OnMessageReceived -= BaseClient_OnMessageReceived;
            if (worker != null) {
                worker.DoWork -= BackgroundWorker_DoWork;
                worker.CancelAsync();
                worker.Dispose();
            }
        }
    }
}

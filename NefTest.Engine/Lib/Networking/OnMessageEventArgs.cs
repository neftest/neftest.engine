﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Lib.Networking {
    public class OnMessageEventArgs : EventArgs {
        public MessageHeader Header { get; }
        public byte[] Body { get; }

        public OnMessageEventArgs(MessageHeader header, byte[] body) {
            Header = header;
            Body = body;
        }
    }
}

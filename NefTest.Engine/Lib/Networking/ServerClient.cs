﻿using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;

namespace NefTest.Engine.Lib.Networking {
    public class ServerClient : ClientBase {
        public ServerClient(int clientId, string connectionId, TcpClient client, ILogger log, Action<Action> runOnMainThread, SerializationBinder binder = null) : base(clientId, connectionId, log, runOnMainThread, binder) {
            SetClient(client);
            IsRemote = true;
        }

        protected override void Dispose(bool isDisposing) {
            base.Dispose(isDisposing);
        }
    }
}

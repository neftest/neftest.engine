﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine.Lib {
    /// <summary>
    /// Container class for running assembly test cases in a new app domain.
    /// </summary>
    public class AssemblyTestRunner : MarshalByRefObject {
        private readonly Dictionary<string, Assembly> _assemblyCache = new Dictionary<string, Assembly>();

        /// <summary>
        /// Creates a new instance of the specified test class and runs the test case specified.
        /// </summary>
        /// <param name="assemblyPath">path to the assembly containing the test case</param>
        /// <param name="className">Fully qualified name of the class</param>
        /// <param name="methodName">Name of the method that represents the test case</param>
        /// <returns></returns>
        public Dictionary<string, string> RunTest(string assemblyPath, string className, string methodName) {
            string output = null;
            string status;

            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            try {
                if (!_assemblyCache.TryGetValue(assemblyPath.ToLower(), out Assembly assembly)) {
                    assembly = Assembly.LoadFrom(assemblyPath);
                    _assemblyCache.Add(assemblyPath.ToLower(), assembly);
                }
                var classType = assembly.GetType(className);
                var instance = Activator.CreateInstance(classType);
                var method = classType.GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance);

                method.Invoke(instance, new object[] { });

                status = "Passed";
            }
            catch (Exception ex) {
                if (ex.InnerException != null)
                    ex = ex.InnerException;
                output = ex.ToString();
                status = "Failed";
            }

            sw.Stop();

            return new Dictionary<string, string> {
                    { "status", status },
                    { "duration", sw.ElapsedTicks.ToString() },
                    { "output", output }
                };
        }
    }
}

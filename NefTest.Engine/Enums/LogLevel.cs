﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Enums {
    /// <summary>
    /// Log Level
    /// </summary>
    public enum LogLevel {
        /// <summary>
        /// Verbose
        /// </summary>
        Verbose = 0,
        /// <summary>
        /// Info
        /// </summary>
        Info = 1,
        /// <summary>
        /// Warning
        /// </summary>
        Warning = 2,
        /// <summary>
        /// Error
        /// </summary>
        Error = 3,
        /// <summary>
        /// Success
        /// </summary>
        Success = 4
    }
}

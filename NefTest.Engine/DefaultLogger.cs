﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    /// <summary>
    /// Default logger implementation, just writes to console.
    /// </summary>
    public class DefaultLogger : ILogger {
        /// <summary>
        /// Log a message to the console with a given log level
        /// </summary>
        /// <param name="message">message to log</param>
        /// <param name="logLevel">log level</param>
        public void Log(string message, LogLevel logLevel = LogLevel.Info) {
            Console.WriteLine($"{logLevel}: {message}");
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="ex">Exception to log</param>
        public void Log(Exception ex) {
            Console.WriteLine($"Exception: {ex}");
        }
    }
}

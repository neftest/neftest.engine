﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class TestCaseResult : ITestCaseResult {
        public string FullyQualifiedName { get; }

        public string ShortName { get; }

        public TestStatus Status { get; }

        public TimeSpan Duration { get; }

        public string Output { get; }

        public ITestCase TestCase { get; }

        public TestCaseResult(string fullyQualifiedName, string shortName, TestStatus status, TimeSpan duration, ITestCase testCase, string output = null) {
            FullyQualifiedName = fullyQualifiedName;
            ShortName = shortName;
            Status = status;
            Duration = duration;
            Output = output;
            TestCase = testCase;
        }

        public int TotalCasesByStatus(TestStatus status) {
            return Status == status ? 1 : 0;
        }

        public int TotalCases() {
            return 1;
        }
    }
}

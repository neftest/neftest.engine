﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class TestClassResult : ITestClassResult {
        private List<ITestCaseResult> _results = new List<ITestCaseResult>();

        public string FullyQualifiedName { get; }

        public string ShortName { get; }

        public ITestClass TestClass { get; }

        public TimeSpan Duration => new TimeSpan(_results.Sum(r => r.Duration.Ticks));

        public TestStatus Status {
            get {
                if (TotalCasesByStatus(TestStatus.Failed) > 0)
                    return TestStatus.Failed;
                else if (TotalCasesByStatus(TestStatus.NotRun) > 0)
                    return TestStatus.NotRun;
                else if (TotalCasesByStatus(TestStatus.Skipped) > 0)
                    return TestStatus.Skipped;
                else if (TotalCasesByStatus(TestStatus.Passed) > 0)
                    return TestStatus.Passed;
                else
                    return TestStatus.NotRun;
            }
        }

        public TestClassResult(string fullyQualifiedName, string shortName, ITestClass testClass) {
            FullyQualifiedName = fullyQualifiedName;
            ShortName = shortName;
            TestClass = testClass;
        }

        public void AddCaseResult(ITestCaseResult result) {
            _results.Add(result);
        }

        public IEnumerable<ITestCaseResult> GetCaseResults() {
            return _results;
        }

        public bool IsPassing() {
            return _results.All(a => a.Status == TestStatus.Passed);
        }

        public int TotalCasesByStatus(TestStatus status) {
            return _results.Where(c => c.Status == status).Count();
        }

        public int TotalCases() {
            return _results.Sum(a => a.TotalCases());
        }
    }
}

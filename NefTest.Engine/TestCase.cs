﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine {
    public class TestCase : ITestCase {
        public MethodInfo MethodInfo { get; private set; }

        private readonly IEngine _engine;
        private readonly ITestClass _parent;

        public string FullyQualifiedName { get => $"{MethodInfo.DeclaringType}.{MethodInfo.Name}"; }

        public EnvironmentType Environment {
            get {
                var thisEnv = ((TestCaseAttribute)MethodInfo.GetCustomAttributes(typeof(TestCaseAttribute), true).First()).EnvironmentType;
                return (thisEnv == EnvironmentType.Any) ? TestClass.Environment : thisEnv;
            }
        }

        public ITestClass TestClass => _parent;

        public TestCase(MethodInfo methodInfo, ITestClass parent, IEngine engine) {
            MethodInfo = methodInfo;
            _engine = engine;
            _parent = parent;
        }

        public ITestCaseResult Run(object testClassInstance) {
            string output = null;
            TestStatus status;

            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            try {
                MethodInfo.Invoke(testClassInstance, new object[] { });
                status = TestStatus.Passed;
            }
            catch (Exception ex) {
                if (ex.InnerException != null)
                    ex = ex.InnerException;
                output = ex.ToString();
                status = TestStatus.Failed;
            }

            sw.Stop();

            return _engine.Resolve<ITestCaseResult>(new Dictionary<object, object> {
                    { "fullyQualifiedName", FullyQualifiedName },
                    { "shortName", MethodInfo.Name },
                    { typeof(TestStatus), status },
                    { typeof(TimeSpan), sw.Elapsed },
                    { "output", output },
                    { typeof(ITestCase), this }
                });
        }
    }
}

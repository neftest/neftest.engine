﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace NefTest.Engine {
    /// <summary>
    /// Exports test results in JUnit format
    /// </summary>
    public class JUnitExporter : ITestExporter {
        /// <summary>
        /// Write the test results to a file in junit format
        /// </summary>
        /// <param name="testResult"></param>
        /// <param name="outFile"></param>
        public void Write(ITestResult testResult, string outFile) {
            var xmlDoc = new XmlDocument();

            var testsuites = xmlDoc.CreateElement("testsuites");
            testsuites.SetAttribute("disabled", testResult.TotalCasesByStatus(Enums.TestStatus.NotRun).ToString());
            testsuites.SetAttribute("errors", testResult.TotalCasesByStatus(Enums.TestStatus.Invalid).ToString());
            testsuites.SetAttribute("failures", testResult.TotalCasesByStatus(Enums.TestStatus.Failed).ToString());
            testsuites.SetAttribute("name", "All Tests");
            testsuites.SetAttribute("tests", testResult.TotalCases().ToString());
            testsuites.SetAttribute("time", testResult.Duration.TotalSeconds.ToString());

            xmlDoc.AppendChild(testsuites);

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                var testsuite = xmlDoc.CreateElement("testsuite");
                testsuite.SetAttribute("tests", testAssembly.TotalCases().ToString());
                testsuite.SetAttribute("disabled", testAssembly.TotalCasesByStatus(Enums.TestStatus.NotRun).ToString());
                testsuite.SetAttribute("errors", testAssembly.TotalCasesByStatus(Enums.TestStatus.Invalid).ToString());
                testsuite.SetAttribute("failures", testAssembly.TotalCasesByStatus(Enums.TestStatus.Failed).ToString());
                testsuite.SetAttribute("skipped", testAssembly.TotalCasesByStatus(Enums.TestStatus.Skipped).ToString());
                testsuite.SetAttribute("hostname", "localhost");
                testsuite.SetAttribute("time", testAssembly.Duration.TotalSeconds.ToString());
                testsuite.SetAttribute("package", testAssembly.FullyQualifiedName);

                testsuites.AppendChild(testsuite);

                foreach (var testClass in testAssembly.GetClassResults()) {

                    foreach (var testCase in testClass.GetCaseResults()) {
                        var testCaseNode = xmlDoc.CreateElement("testcase");
                        testCaseNode.SetAttribute("name", testCase.ShortName);
                        testCaseNode.SetAttribute("classname", testClass.FullyQualifiedName);
                        testCaseNode.SetAttribute("time", testCase.Duration.TotalSeconds.ToString());

                        testsuite.AppendChild(testCaseNode);

                        switch (testCase.Status) {
                            case TestStatus.Failed:
                                var failure = xmlDoc.CreateElement("failure");
                                failure.SetAttribute("message", testCase.Output);
                                failure.InnerText = testCase.Output;
                                testCaseNode.AppendChild(failure);
                                break;
                            case TestStatus.Skipped:
                            case TestStatus.NotRun:
                                var skipped = xmlDoc.CreateElement("skipped");
                                skipped.SetAttribute("message", "Skipped");
                                testCaseNode.AppendChild(skipped);
                                break;
                            case TestStatus.Invalid:
                                var invalid = xmlDoc.CreateElement("error");
                                invalid.SetAttribute("message", testCase.Output);
                                testCaseNode.AppendChild(invalid);
                                break;
                        }
                    }
                }
            }

            using (var writer = new XmlTextWriter(outFile, Encoding.UTF8)) {
                xmlDoc.WriteTo(writer);
            }
        }
    }
}

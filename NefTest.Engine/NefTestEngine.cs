﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using NefTest.Engine.Lib.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace NefTest.Engine {
    /// <summary>
    /// The default NefTest IEngine implementation
    /// </summary>
    public class NefTestEngine : IEngine {
        private readonly Dictionary<Type, EngineComponent> _registeredComponents = new Dictionary<Type, EngineComponent>();
        private readonly bool shouldExit = true;

        /// <summary>
        /// Reference to the ITestRunner instance being used
        /// </summary>
        public ITestRunner TestRunner => Resolve<ITestRunner>();

        /// <summary>
        /// Reference to the ITestFinder being used
        /// </summary>
        public ITestFinder TestFinder => Resolve<ITestFinder>();

        /// <summary>
        /// Reference to the ITestReporter being used
        /// </summary>
        public ITestReporter TestReporter => Resolve<ITestReporter>();

        /// <summary>
        /// Reference to the ITestExporter being used
        /// </summary>
        public ITestExporter TestExporter => Resolve<ITestExporter>();

        private ILogger Logger => Resolve<ILogger>();

        /// <summary>
        /// Reference to the NefTestServer instance
        /// </summary>
        public NefTestServer Server { get; private set; }

        /// <summary>
        /// Initialize the engine. Make sure custom components are registered before calling this.
        /// </summary>
        public void Initialize() {
            // Engines should register themselves. This allows other components to have access
            // to the IoC container.
            RegisterComponent(typeof(IEngine), typeof(NefTestEngine), true, this);

            // default interface implementations
            if (!_registeredComponents.ContainsKey(typeof(ILogger)))
                RegisterComponent(typeof(ILogger), typeof(DefaultLogger), true);
            if (!_registeredComponents.ContainsKey(typeof(ITestRunner)))
                RegisterComponent(typeof(ITestRunner), typeof(DefaultTestRunner), true);
            if (!_registeredComponents.ContainsKey(typeof(ITestAssembly)))
                RegisterComponent(typeof(ITestAssembly), typeof(TestAssembly));
            if (!_registeredComponents.ContainsKey(typeof(ITestClass)))
                RegisterComponent(typeof(ITestClass), typeof(TestClass));
            if (!_registeredComponents.ContainsKey(typeof(ITestCase)))
                RegisterComponent(typeof(ITestCase), typeof(TestCase));
            if (!_registeredComponents.ContainsKey(typeof(ITestResult)))
                RegisterComponent(typeof(ITestResult), typeof(TestResult));
            if (!_registeredComponents.ContainsKey(typeof(ITestAssemblyResult)))
                RegisterComponent(typeof(ITestAssemblyResult), typeof(TestAssemblyResult));
            if (!_registeredComponents.ContainsKey(typeof(ITestClassResult)))
                RegisterComponent(typeof(ITestClassResult), typeof(TestClassResult));
            if (!_registeredComponents.ContainsKey(typeof(ITestCaseResult)))
                RegisterComponent(typeof(ITestCaseResult), typeof(TestCaseResult));
            if (!_registeredComponents.ContainsKey(typeof(ITestReporter)))
                RegisterComponent(typeof(ITestReporter), typeof(LoggerTestReporter), true);
            if (!_registeredComponents.ContainsKey(typeof(ITestExporter)))
                RegisterComponent(typeof(ITestExporter), typeof(JUnitExporter), true);
        }

        #region IEngine Implementation
        /// <summary>
        /// Registering a registerType that already exists will overwrite the old implementation. This allows for selectively
        /// overriding different interfaces with a custom implementation where needed.
        /// </summary>
        /// <param name="registerType"></param>
        /// <param name="implementedType"></param>
        /// <param name="isSingleton"></param>
        /// <param name="instance"></param>
        public void RegisterComponent(Type registerType, Type implementedType, bool isSingleton=false, object instance=null) {
            //Logger?.Log($"Register {implementedType} as {registerType} isSingleton:{isSingleton} instance:{instance}", LogLevel.Verbose);
            // remove any existing implementations of registerType
            UnregisterComponent(registerType);

            _registeredComponents.Add(registerType, new EngineComponent() {
                ImplementedType = implementedType,
                RegisteredType = registerType,
                IsSingleton = isSingleton,
                Instance = instance
            });
        }

        /// <summary>
        /// Unregister an engine component
        /// </summary>
        /// <param name="registerType"></param>
        public void UnregisterComponent(Type registerType) {
            if (_registeredComponents.ContainsKey(registerType))
                _registeredComponents.Remove(registerType);
        }

        /// <summary>
        /// Resolve engine component of type T
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <returns></returns>
        public T Resolve<T>() where T : class {
            if (_registeredComponents.TryGetValue(typeof(T), out EngineComponent component)) {
                return (T)GetInstance(component);
            }

            if (!(typeof(ILogger).IsAssignableFrom(typeof(T))))
                Logger?.Log($"Resolve {typeof(T)} as {component.ImplementedType} WAS NULL", LogLevel.Warning);

            return null;
        }

        /// <summary>
        /// Resolve engine component of type T with constructor parameters.
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <param name="parameterOverloads">Constructor parameters</param>
        /// <returns></returns>
        public T Resolve<T>(IDictionary<object, object> parameterOverloads) where T : class {
            if (_registeredComponents.TryGetValue(typeof(T), out EngineComponent component)) {
                return (T)CreateInstance(component, parameterOverloads);
            }

            if (!(typeof(ILogger).IsAssignableFrom(typeof(T))))
                Logger?.Log($"Resolve {typeof(T)} as {component.ImplementedType} WAS NULL", LogLevel.Warning);

            return null;
        }
        #endregion // IEngine Implementation

        private object GetInstance(EngineComponent component) {
            if (component.IsSingleton && component.Instance != null)
                return component.Instance;

            return CreateInstance(component);
        }

        private object CreateInstance(EngineComponent component, IDictionary<object, object> parameterOverloads=null) {
            if (component.RegisteredType != typeof(ILogger))
                Logger?.Log($"CreateInstance: {component.ImplementedType}//{component.RegisteredType}: {component.IsSingleton} // {component.Instance}", LogLevel.Verbose);

            if (component.IsSingleton && component.Instance != null)
                return component.Instance;

            object newInstance;
            object[] args = BuildArgs(component, parameterOverloads);

            if (args == null) {
                newInstance = Activator.CreateInstance(component.ImplementedType);
            }
            else {
                newInstance = Activator.CreateInstance(component.ImplementedType, args);
            }

            if (component.IsSingleton)
                component.Instance = newInstance;

            return newInstance;
        }

        private object[] BuildArgs(EngineComponent component, IDictionary<object, object> parameterOverloads) {
            var constructors = component.ImplementedType.GetConstructors().ToList();
            
            // sort constructors by largest number of params first
            constructors.Sort((a, b) => -a.GetParameters().Count().CompareTo(b.GetParameters().Count()));

            foreach (var constructor in constructors) {
                if (TrySatisfyConstructorParams(parameterOverloads, constructor, out List<object> constructorArgs)) {
                    return constructorArgs.ToArray();
                }
            }

            return null;
        }

        private bool TrySatisfyConstructorParams(IDictionary<object, object> parameterOverloads, ConstructorInfo constructor, out List<object> constructorArgs) {
            var args = new List<object>();
            bool foundAllParams = true;
            IEnumerable<string> namedParameterKeys = new string[] { };
            IEnumerable<Type> typedParameterKeys = new Type[] { };

            if (parameterOverloads != null) {
                namedParameterKeys = parameterOverloads.Keys.Where(k => k is string).Select(k => k.ToString());
                typedParameterKeys = parameterOverloads.Keys.Where(k => k is Type).Select(k => (Type)k);
            }

            foreach (var param in constructor.GetParameters()) {
                // check for a namedParameter match
                if (namedParameterKeys.Contains(param.Name)) {
                    args.Add(parameterOverloads[param.Name]);
                    continue;
                }
                // check for a typedParameter match
                else if (typedParameterKeys.Contains(param.ParameterType)) {
                    args.Add(parameterOverloads[param.ParameterType]);
                    continue;
                }
                // check out existing component registry
                else if (_registeredComponents.TryGetValue(param.ParameterType, out EngineComponent paramComponent)) {
                    args.Add(GetInstance(paramComponent));
                    continue;
                }

                foundAllParams = false;
                break;
            }

            constructorArgs = foundAllParams ? args : new List<object>();
            return foundAllParams;
        }

        /// <summary>
        /// Attempt to start a listening server for test runner instance communication.
        /// This may fail because another runner may already be listening. If so you should
        /// attempt to reconnect as a client.
        /// </summary>
        /// <param name="port">port to listen on</param>
        /// <returns>success</returns>
        public bool TryStartServer(int port) {
            return true;
            /*
            Logger?.Log($"Starting listening server on port: {port}");

            bool isRunning = false;
            using (var mutex = new Mutex(false, "com.NefTest.Server.Instance")) {
                string host = "127.0.0.1";
                bool isAnotherInstanceOpen = !mutex.WaitOne(TimeSpan.Zero);
                if (isAnotherInstanceOpen) {
                    Logger?.Log("Unable to start NefTestServer. Only one instance is allowed.", LogLevel.Warning);
                    return false;
                }

                var t = Task.Factory.StartNew(() => {
                    Server = new NefTestServer(host, port, Logger);
                    isRunning = true;
                    while (!shouldExit) {
                        Thread.Sleep(100);
                    }
                    Server?.Dispose();
                });

                while (!isRunning) {
                    Thread.Sleep(10);
                }

                return true;
            }
            */
        }
    }
}

﻿using NefTest.Common.Attributes;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Tests {
    [TestCategory]
    public class EngineTests {
        [TestCase]
        public void Can_Create_Instance() {
            var engine = new NefTestEngine();

            engine.ShouldNotBeNull();
        }
    }
}
